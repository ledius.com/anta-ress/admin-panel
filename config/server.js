module.exports = ({ env }) => ({
  host: env('HOST', 'admin-panel.anta-ress.ru'),
  port: env.int('PORT', 3000),
  app: {
    keys: env.array('APP_KEYS'),
  },
  url: env('APP_URL', 'https://admin-panel.anta-ress.ru'),
});
