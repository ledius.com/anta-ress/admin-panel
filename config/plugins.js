module.exports = ({env}) => ({
  'html-editor': {
    enabled: true,
    resolve: './src/plugins/html-editor'
  },
  seo: {
    enabled: true,
  },

  upload: {
    config: {
      provider: 'aws-s3',
      providerOptions: {
        accessKeyId: env('AWS_ACCESS_KEY_ID'),
        secretAccessKey: env('AWS_ACCESS_SECRET'),
        // region: env('AWS_REGION'),
        endpoint: env('AWS_URL', 'https://storage.yandexcloud.net'),
        s3ForcePathStyle: true,
        apiVersion: 'latest',
        params: {
          Bucket: env('AWS_BUCKET'),
          ACL:'public-read'
        },
        logger: console
      },
    }
  },

  "entity-relationship-chart": {
    enabled: true,
    config: {
      // By default all contentTypes and components are included.
      // To exlclude strapi's internal models, use:
      exclude: [
        "strapi::core-store",
        "webhook",
        "admin::permission",
        "admin::user",
        "admin::role",
        "admin::api-token",
        "plugin::upload.file",
        "plugin::i18n.locale",
        "plugin::users-permissions.permission",
        "plugin::users-permissions.role",
      ],
    },
  },
})
