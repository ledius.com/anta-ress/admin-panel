module.exports = ({ env }) => ({
  auth: {
    secret: env('ADMIN_JWT_SECRET', '38a2d36befb8d6d85084017be7193f5b'),
  },
  autoOpen: false,
  url: env('APP_URL', 'https://admin-panel.anta-ress.ru') + '/admin',
  host: env('HOST', 'admin-panel.anta-ress.ru'),
  port: env.int('PORT', 3000),
});
