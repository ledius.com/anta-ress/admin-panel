'use strict';

/**
 *  prikaz controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::prikaz.prikaz');
