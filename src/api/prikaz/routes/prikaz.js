'use strict';

/**
 * prikaz router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::prikaz.prikaz');
