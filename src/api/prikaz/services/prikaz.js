'use strict';

/**
 * prikaz service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::prikaz.prikaz');
