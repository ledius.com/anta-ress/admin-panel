'use strict';

/**
 * uslugi router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::uslugi.uslugi');
