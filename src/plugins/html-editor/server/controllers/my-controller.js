'use strict';

module.exports = {
  index(ctx) {
    ctx.body = strapi
      .plugin('html-editor')
      .service('myService')
      .getWelcomeMessage();
  },
};
